/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package passwordvalidator;

/**
 *
 * @author Rachit
 */
public class Test {
    public static void main(String[] args) {
		String password1 = "passWorddY";
		String password2 = "Qwerty12";
		String password3 = "p4ssw@rD";
		String password4 = "HoW You D1ing";
		
		test(password1);
		test(password2);
		test(password3);
		test(password4);
	}
	public static void test(String password) {
		PasswordValidator validator = new PasswordValidator();
		if(validator.validate(password)) {
			System.out.println(password + " is a strong password.");
		}else {
			System.out.println(password + " is a weak password.");
		}
	}
}
